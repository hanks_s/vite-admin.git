import { createRouter, createWebHashHistory } from 'vue-router';

/**
 * 布局路由
 * */
export const baseRoutes = [
  {
    path: '/',
    name: 'index',
    component: () => import('@/layout/index.vue'),
    redirect: '/home',
    meta: {
      isKeepAlive: true
    },
    children: [
      {
        path: '/home',
        name: 'home',
        component: () => import('@/views/home/index.vue'),
        meta: {
          isKeepAlive: true,
          title: '首页'
        }
      }
    ]
  }
];

/**
 * 定义静态路由（默认路由）
 */
export const staticRoutes = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/login/index.vue'),
    meta: {
      title: '登录'
    }
  },
  {
    path: '/:pathMatch(.*)*', // 解决路由爆[Vue Router warn]: No match found for location with path
    meta: {
      title: '找不到此页面'
    },
    // redirect: '/403', // 错误方式，刷新立马会导致进入守卫的页面
    component: () => import('@/views/error/404.vue') // 切记不要使用 redirect: '/403',
  }
];

export const router = createRouter({
  history: createWebHashHistory(),
  base: '/admin2/',
  /**
   * 说明：
   * 1、notFoundAndNoPower 默认添加 404、401 界面，防止一直提示 No match found for location with path 'xxx'
   * 2、backEnd.ts(后端控制路由)、frontEnd.ts(前端控制路由) 中也需要加 notFoundAndNoPower 404、401 界面。
   *    防止 404、401 不在 layout 布局中，不设置的话，404、401 界面将全屏显示
   */
  routes: [...staticRoutes, ...baseRoutes]
});

export default router;
