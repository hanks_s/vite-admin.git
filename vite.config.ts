import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
// import eslintPlugin from 'vite-plugin-eslint';
import AutoImport from 'unplugin-auto-import/vite';
import { resolve } from 'path';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    AutoImport({
      imports: ['vue', 'vue-router', 'pinia'], // 自动导入的依赖库数组
      dts: './auto-imports.d.ts' // 自动导入类型定义文件路径
      // eslint报错解决  https://blog.csdn.net/m0_47119963/article/details/130778027
      // eslintrc: {
      //   enabled: false, // Default `false`
      //   filepath: './src/types/.eslintrc-auto-import.json', // Default `./.eslintrc-auto-import.json`
      //   globalsPropValue: true // Default `true`, (true | false | 'readonly' | 'readable' | 'writable' | 'writeable')
      // }
    })
    // 配置vite在运行的时候自动检测eslint规范，注释不检测
    // eslintPlugin({
    //   include: ['src/**/*.ts', 'src/**/*.js', 'src/**/*.vue', 'src/*.ts', 'src/*.js', 'src/*.vue']
    // })
  ],
  resolve: {
    alias: {
      '@': resolve(__dirname, './src'),
      '*': resolve('')
    }
  }
});
